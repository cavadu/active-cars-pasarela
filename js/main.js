function videosH() {
	$(".btn_vid").hover(function() {
		TweenLite.to($(this).find(".c_hover"), 0.5, {top:"10%", ease:Expo.easeInOut});
		/*$(this).find("img").stop().animate({
			"width": "140%",
			"top": "-20%",
			"left": "-20%"
		}, 250)*/
	}, function() {
		TweenLite.to($(this).find(".c_hover"), 0.5, {top:"-150%", ease:Expo.easeInOut});
		/*$(this).find("img").stop().animate({
			"top": "0",
			"left": "0%",
			"width": "100%"
		}, 250)*/
	});	

	$(".menu_int .list").hover(function() {
		TweenLite.to($(this).find(".btn_menu"), 0.5, {top:"0.5fem", ease:Expo.easeInOut});
	}, function() {
		TweenLite.to($(this).find(".btn_menu"), 0.5, {top:0, ease:Expo.easeInOut});
	});	
}

function lBox() {
	$("#h_donacion, .h_btn1, .h_call a, .h_hazD").bind('click', function() {
		TweenLite.to($(".h_lb"), 1, {top:0, ease:Expo.easeInOut});
		$(".h_valores li").removeClass("h_listAct");
	});
	$(".h_lbfull, .h_lb .h_cerrar").bind('click', function() {
		TweenLite.to($(".h_lb"), 1, {top:"-300%", ease:Expo.easeInOut, delay:0.5});
	});
	$(".h_valores li").bind('click', function() {
		$(".h_valores li").removeClass("h_listAct");
		$(this).addClass("h_listAct");
	});
}

function cerrarLB() {
	$(".c_lb .c_cerrar").bind("click", function() {
		TweenLite.to($(".c_lb"), 0.5, {top:"-300%", ease:Expo.easeInOut});
	})
}

function openVid() {
	$(".btn_vid").bind("click", function() {
		TweenLite.to($(this).parent().find(".h_vidInter"), 0.5, {top:0, ease:Expo.easeInOut});
		 
	})
	$('.h_vidInter .c_cerrar').on('click', function() {
		$(this).parent().find('iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');    
	});
}


$(document).ready(function() {
	cerrarLB();
	videosH();
	openVid();

	$(".btn_action").click(function(event) {
		if ($(this).hasClass('cerrarMob')) {
			$(this).parent().find('.c_menu').slideUp('slow');
			$(this).removeClass('cerrarMob');
		} else {
			$(this).addClass('cerrarMob');	
			$(this).parent().find('.c_menu').slideDown('slow', function() {});
		}
	});

	$(".c_fix").hover(function() {
		TweenLite.to($(".c_fix"), 0.5, {right:0, ease:Expo.easeInOut});
	}, function() {
		TweenLite.to($(".c_fix"), 0.5, {right:"-190px", ease:Expo.easeInOut});
	});	
});

$( window ).resize(function(){

});